#include "Context.h"
#include "CSVMaker.h"
#include "Reader.h"
#include "StringParser.h"
#include <iostream>
#include "Consts.h"

int main(int argc, char** argv) {
	
	if (argc != Consts::MAX_NUM_ARGS) {
		std::cout << Consts::ERROR_TEXT_3 << std::endl;
		return Consts::EXIT_CODE;
	}
	lab0::Context* context = new lab0::Context;
	lab0::StringParser* string_parser = new lab0::StringParser(context);
	
	lab0::Reader* input = nullptr;
	try {
		input = new lab0::Reader(argv[Consts::INPUT_FILE_ARG], string_parser);
	}
	catch (std::wstring error) {
		std::wcout << error << std::endl;
		if (input != nullptr) {
			delete input;
		}
		return Consts::EXIT_CODE;
	}
	input->read_file();
	delete string_parser;
	delete input;
	lab0::CSVMaker* output = nullptr;
	try {
		output = new lab0::CSVMaker(context, argv[Consts::OUTPUT_FILE_ARG]);
	}
	catch(std::ios::failure){
		std::cout << Consts::ERROR_TEXT << std::endl;
		if (output != nullptr) {
			delete output;
		}
		return Consts::EXIT_CODE;
	}
	output->make_output_file();
	delete context;
	delete output;
	return Consts::EXIT_CODE;
}