#pragma once
#include <string>
namespace Consts {
	static const int MAX_NUM_ARGS = 3;
	static const std::wstring FIRST_COLUMN_NAME = L"word";
	static const std::wstring SECOND_COLUMN_NAME = L"amount";
	static const std::wstring THIRD_COLUMN_NAME = L"frequency";
	static const std::wstring EMPTY_STRING = L"";
	static const wchar_t ENG_ALPH_START_LOW = L'a';
	static const wchar_t ENG_ALPH_END_LOW = L'z';
	static const wchar_t ENG_ALPH_START_HIGH = L'A';
	static const wchar_t ENG_ALPH_END_HIGH = L'Z';
	static const wchar_t RUS_ALPH_START_LOW = L'�';
	static const wchar_t RUS_ALPH_END_LOW = L'�';
	static const wchar_t RUS_ALPH_START_HIGH = L'�';
	static const wchar_t RUS_ALPH_END_HIGH = L'�';
	static const wchar_t NUM_ALPH_START = L'0';
	static const wchar_t NUM_ALPH_END = L'9';
	static const wchar_t ENDL = L'\n';
	static const std::string ERROR_TEXT = "can't find folder, bad pass";
	static const int EXIT_CODE = 0;
	static const std::wstring ERROR_TEXT_2 = L"bad input, can't open file";
	static const std::string ERROR_TEXT_3 = "bad num of args";
	static const std::wstring SEPARATOR = L",";
	static const int COUNTDOWN_START = 0;
	static const int INPUT_FILE_ARG = 1;
	static const int OUTPUT_FILE_ARG = 2;
};