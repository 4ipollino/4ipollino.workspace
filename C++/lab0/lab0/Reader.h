#pragma once
#include <string>
#include <fstream>
#include "StringParser.h"

namespace lab0 {

	class Reader {
	private:
		std::string file_name_;
		std::wifstream input_;
		StringParser* string_parser_;
	public:
		Reader(std::string file_name, StringParser* string_parser);

		~Reader();

		void read_file();
	};
};
