﻿#include "StringParser.h"
#include <iostream>
#include "Consts.h"
lab0::StringParser::StringParser(Context* context) {
	this->context_ = context;
}
void lab0::StringParser::parse_string(std::wstring str) {
	std::wstring word = Consts::EMPTY_STRING;
	for (int i = 0; i < str.length(); ++i) {
		if (Consts::ENG_ALPH_START_LOW <= str[i] && str[i] <= Consts::ENG_ALPH_END_LOW ||
			Consts::ENG_ALPH_START_HIGH <= str[i] && str[i] <= Consts::ENG_ALPH_END_HIGH ||
			Consts::RUS_ALPH_START_LOW <= str[i] && str[i] <= Consts::RUS_ALPH_END_LOW ||
			Consts::RUS_ALPH_START_HIGH <= str[i] && str[i] <= Consts::RUS_ALPH_END_HIGH ||
			Consts::NUM_ALPH_START <= str[i] && str[i] <= Consts::NUM_ALPH_END) {

			word += str[i];
		}
		else {
			if (!word.length()) {
				continue;
			}
			else {
				this->context_->add_word(word);
				word = Consts::EMPTY_STRING;
			}
		}
	}
}