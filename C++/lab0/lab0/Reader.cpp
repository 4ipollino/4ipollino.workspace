#include "Reader.h"
#include <codecvt>
#include "Consts.h"

lab0::Reader::Reader(std::string file_name, StringParser *string_parser) {
	this->file_name_ = file_name;
	const std::locale utf8_locale = std::locale(std::locale(), new std::codecvt_utf8<wchar_t>());
	this->input_.open(this->file_name_);
	this->input_.imbue(utf8_locale);
	this->string_parser_ = string_parser;
	if (!this->input_.is_open()) {
		std::wstring error = Consts::ERROR_TEXT_2;
		throw error;
	}
}

lab0::Reader::~Reader() {
	this->input_.close();
}

void lab0::Reader::read_file() {
	std::wstring str;
	while (getline(this->input_, str)) {
		this->string_parser_->parse_string(str + Consts::ENDL);
	}
}


