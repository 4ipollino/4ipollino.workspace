#include "Context.h"
#include <algorithm>
#include "Consts.h"

lab0::Context::Context() {
	this->word_counter = 0;
	this->it = stat.begin();
}

void lab0::Context::add_word(std::wstring word) {
	this->word_counter++;
	if (this->stat.find(word) == stat.end()) {
		this->stat[word] = 1;
	}
	else {
		this->stat[word]++;
	}
}

lab0::Context::~Context() {
	this->stat.clear();
}

int lab0::Context::give_word_counter() {
	 return this->word_counter;
}

std::vector<std::pair<std::wstring, int>> lab0::Context::give_sorted_words() {
	std::pair<std::wstring, int> pair;
	std::vector<std::pair<std::wstring, int>> vector_of_pairs;
	this->it = this->stat.begin();
	for (int i = Consts::COUNTDOWN_START; i < this->stat.size(); ++i) {
		pair = make_pair(this->it->first, this->it->second);
		vector_of_pairs.push_back(pair);
		this->it++;
	}
	std::sort(vector_of_pairs.begin(), vector_of_pairs.end(), [](auto& left, auto& right) {
		return left.second > right.second;
	});
	return vector_of_pairs;
}

