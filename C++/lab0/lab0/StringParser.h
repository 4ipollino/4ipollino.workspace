#pragma once

#include <string>
#include "Context.h"

namespace lab0 {

	class StringParser {
	private:
		Context* context_;
	public:
		StringParser(Context* context);
		void parse_string(std::wstring str);

	};
};

