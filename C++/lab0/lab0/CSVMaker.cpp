#include "CSVMaker.h"
#include <iostream>
#include <codecvt>
#include "Consts.h"

lab0::CSVMaker::CSVMaker(Context* context, const std::string file_name) {
	this->context_ = context;
	this->file_name_ = file_name;
	const std::locale utf8_locale = std::locale(std::locale(), new std::codecvt_utf8<wchar_t>());
	this->output_.open(this->file_name_);
	this->output_.imbue(utf8_locale);
	this->output_.exceptions(std::ios::failbit | std::ios::badbit);
}

lab0::CSVMaker::~CSVMaker() {
	this->output_.close();
}

void lab0::CSVMaker::print_headers() {
	this->output_ << Consts::FIRST_COLUMN_NAME << Consts::SEPARATOR << Consts::SECOND_COLUMN_NAME << Consts::SEPARATOR << Consts::THIRD_COLUMN_NAME << std::endl;
}

double lab0::CSVMaker::get_frequency(int word_counter, int frequency) {
	double common_frequency = (double)frequency / (double)word_counter  ;
	return common_frequency;
}

void lab0::CSVMaker::write(std::wstring word, int amount) {
	int word_counter = this->context_->give_word_counter();
	double frequency = get_frequency(word_counter, amount);
	this->output_ << word << Consts::SEPARATOR << std::to_wstring(amount) << Consts::SEPARATOR << std::to_wstring(frequency) << std::endl;
}

void lab0::CSVMaker::make_output_file() {
	print_headers();
	std::vector<std::pair<std::wstring, int>> words = this->context_->give_sorted_words();
	for (std::vector<std::pair<std::wstring, int>>::iterator it = words.begin(); it != words.end(); it++) {
		write(it->first, it->second);
	}
}

