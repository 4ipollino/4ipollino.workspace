#pragma once

#include <map>
#include <string>
#include <vector>

namespace lab0 {

	class Context {
	private:
		std::map <std::wstring, int> stat;
		std::map <std::wstring, int>::iterator it;
		int word_counter;
	public:
		Context();
		~Context();

		void add_word(std::wstring word);
		int give_word_counter();
		std::vector<std::pair<std::wstring, int>>give_sorted_words();
	};
};

