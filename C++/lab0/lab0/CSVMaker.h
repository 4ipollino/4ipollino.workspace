#pragma once
#include "Context.h"
#include <fstream>

namespace lab0 {

	class CSVMaker {
	private:
		Context* context_;
		std::string file_name_;
		std::wofstream output_;
	public:
		CSVMaker(Context* context, const std::string file_name);
		~CSVMaker();
		void make_output_file();
		void write(std::wstring word, int amount);
		double get_frequency(int word_counter, int frequency);
		void print_headers();
	};
};
