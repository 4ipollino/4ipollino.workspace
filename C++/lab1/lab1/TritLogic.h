#pragma once
#include <iostream>

namespace TritLogic {
	enum Trit {
		FALSE = 1,
		UNKNOWN = 0,
		TRUE = 2,
	};

	Trit operator ~ (const Trit trit);
	Trit operator & (const Trit trit_1, const Trit trit_2);
	Trit operator | (const Trit trit_1, const Trit trit_2);
};