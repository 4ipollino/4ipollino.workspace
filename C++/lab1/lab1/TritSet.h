#pragma once
#include "TritLogic.h"
#include <vector>
#include <unordered_map>

namespace TritLogic {
	
	typedef unsigned int uint;	
	
	class TritSet {
	private:
		//tritset fields
		std::vector<uint> _container;
		size_t _container_size;
		size_t _last_significant_trit;
		size_t _vector_size;

		size_t _true_counter = 0;
		size_t _false_counter = 0;
		size_t _unknown_counter = 0;
		
		//getters
		Trit _get_trit(size_t index) const;
		long long _get_vector_size(long long size);
		long long _search_last_index() const;
		//setters
		void _set_trit(Trit trit, size_t index);
		void _reallocate_trit_set(long long index);
	
	public:
		class TritReference {
			
			friend class TritSet;

		private:
			//reference fields
			TritSet& _trit_set;
			size_t _position;

			//constructor
			TritReference(TritSet& trit, size_t position);

		public:
			//overloading =
			TritSet& operator = (Trit trit);

			//type conversion
			operator Trit() const;

		};
		//setters
		size_t size() const;
		
		//getters
		size_t cardinality(Trit trit) const;

		//overloading []
		TritReference operator [] (size_t position);
		Trit operator [] (size_t position) const;

		//constructor
		TritSet(size_t = 0, Trit = UNKNOWN);

		//copy constructor
		TritSet(const TritSet& trit_set);
		//overloading copy =
		TritSet& operator = (const TritSet& trit_set);

		//move constructor
		TritSet(TritSet&& trit_set);
		//overloading move =
		TritSet& operator = (TritSet&& trit_set);
		//some additional methods
		size_t capacity() const;
		void shrink();
		void trim(size_t position);
	
		//cardinality - for all values
		std::unordered_map<Trit, size_t, std::hash<size_t>> cardinality() const;

	};

	TritSet operator ~ (const TritSet& trit_set);
	TritSet operator | (const TritSet& trit_set_1, const TritSet& trit_set_2);
	TritSet operator & (const TritSet& trit_set_1, const TritSet& trit_set_2);

	bool operator == (const TritSet& trit_set_1, const TritSet& trit_set_2);
	bool operator != (const TritSet& trit_set_1, const TritSet& trit_set_2);
};

