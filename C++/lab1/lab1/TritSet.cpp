#include "TritSet.h"
#include "Consts.h"
#include <iostream>

namespace TritLogic {
	//realloc
	//----------------------------------------------------------------------------------------------------------------------------------------
	void TritSet::_reallocate_trit_set(long long index) {
		
		size_t new_vector_size = _get_vector_size(index);
		if (new_vector_size > _vector_size)
		{
			for (size_t i = 0; i < new_vector_size - _vector_size; i++) {
				_container.push_back(0);
			}
		}
		else
		{
			for (size_t i = 0; i < _vector_size - new_vector_size; i++) {
				_container.pop_back();
			}
		}
		_vector_size = new_vector_size;
		_container_size = index + Consts::LEVELER;
	}
	//----------------------------------------------------------------------------------------------------------------------------------------
	
	//getters
	//----------------------------------------------------------------------------------------------------------------------------------------
	size_t TritSet::capacity() const {
		return _container_size;
	}
	
	long long TritSet::_get_vector_size(long long size) {
		if (size < 0) {
			return 0;
		}
		return size * Consts::TRIT_SIZE / Consts::BYTE_SIZE / Consts::UINT_SIZE + Consts::LEVELER;
	}
	
	size_t TritSet::size() const {
		return _last_significant_trit;
	}

	Trit TritSet::_get_trit(size_t index) const {
		if (index >= _container_size) {
			return UNKNOWN;
		}

		return Trit((_container[index / (Consts::UINT_SIZE * Consts::BYTE_SIZE / Consts::TRIT_SIZE)] >> ((index % (Consts::UINT_SIZE * Consts::BYTE_SIZE / Consts::TRIT_SIZE)) * Consts::TRIT_SIZE)) & Consts::MASK);

	}
	
	long long TritSet::_search_last_index() const {
		long long last_index = -1;
		for (size_t i = 0; i < _container_size; i++) {
			if (_get_trit(i) != UNKNOWN) {
				last_index = i;
			}
		}
		return last_index;
	}
	//----------------------------------------------------------------------------------------------------------------------------------------
	
	//setters
	//----------------------------------------------------------------------------------------------------------------------------------------
	void TritSet::_set_trit(Trit trit, size_t position) {
		if (position >= _container_size) {
			if (trit == UNKNOWN) {
				return;
			}
			_reallocate_trit_set(position);
		}
		//some magic with bits
		size_t container_position = position / (Consts::UINT_SIZE * Consts::BYTE_SIZE / Consts::TRIT_SIZE);
		uint trit_position = position % (Consts::UINT_SIZE * Consts::BYTE_SIZE / Consts::TRIT_SIZE);
		size_t previous_trit = _container[container_position] >> (trit_position * Consts::TRIT_SIZE) & Consts::MASK;
		uint trit_with_shift = ((uint)trit << (trit_position * Consts::TRIT_SIZE));
		
		if (previous_trit == TRUE) {
			_true_counter--;
		}
		else if (previous_trit == FALSE){ 
			_false_counter--;
		}
		else {
			_unknown_counter--;
		}

		if (trit == TRUE) {
			_true_counter++;
		}
		else if (trit == FALSE) {
			_false_counter++;
		}
		else {
			_unknown_counter++;
		}

		//rewritting trit		
		_container[container_position] = static_cast<uint>(((~(Consts::MASK << (trit_position * Consts::TRIT_SIZE))) & _container[container_position]) | trit_with_shift);

		if (position >= _last_significant_trit && trit != UNKNOWN) {
			_last_significant_trit = position + Consts::LEVELER;
		}

		if (trit == UNKNOWN) {
			_last_significant_trit = _search_last_index() + Consts::LEVELER;
		}
		//std::cout << "trit" << _get_trit(index) << " ";

	}
	//----------------------------------------------------------------------------------------------------------------------------------------
	
	//type conversion
	//----------------------------------------------------------------------------------------------------------------------------------------
	TritSet::TritReference::operator Trit() const {
		return _trit_set._get_trit(_position);
	}
	//----------------------------------------------------------------------------------------------------------------------------------------
	
	//constructors
	//----------------------------------------------------------------------------------------------------------------------------------------
	//trit_set constructor
	TritSet::TritSet(size_t size, Trit trit) : _container_size(size), _last_significant_trit(trit == UNKNOWN ? 0 : size) {

		if (trit == TRUE) {
			_true_counter = size;
		}
		else if (trit == FALSE) {
			_false_counter = size;
		}
		else {
			_unknown_counter = size;
		}
		_vector_size = size ? _get_vector_size(size) : 0;

		uint mask = 0;

		for (size_t i = 0; i < Consts::UINT_SIZE * Consts::BYTE_SIZE / Consts::TRIT_SIZE; i++)
			mask |= trit << i * Consts::TRIT_SIZE;


		for (size_t i = 0; i < _vector_size; i++)
			_container.push_back(mask);
		if (size) {
			this->trim(_container_size - Consts::LEVELER);
		}
	}
	
	//copy constructor
	TritSet::TritSet(const TritSet& trit_set): 
		_vector_size(trit_set._vector_size),
		_last_significant_trit(trit_set._last_significant_trit),
		_container_size(trit_set._container_size),
		
		_true_counter(trit_set._true_counter),
		_false_counter(trit_set._false_counter),
		_unknown_counter(trit_set._unknown_counter)
	{
		for (size_t i = 0; i < _vector_size; ++i) {
			_container.push_back(0);
		}

		for (size_t i = 0; i < _last_significant_trit; ++i) {
			_set_trit(trit_set._get_trit(i), i);
		}
	}

	//move constructor
	TritSet::TritSet(TritSet&& trit_set) :
		_vector_size(trit_set._vector_size),
		_last_significant_trit(trit_set._last_significant_trit),
		_container_size(trit_set._container_size),
		_container(trit_set._container),
		
		_true_counter(trit_set._true_counter),
		_false_counter(trit_set._false_counter),
		_unknown_counter(trit_set._unknown_counter)
	{
		trit_set._vector_size = 0;
		trit_set._last_significant_trit = 0;
		trit_set._container_size = 0;

		trit_set._true_counter = 0;
		trit_set._false_counter = 0;
		trit_set._unknown_counter = 0;
		trit_set._container.clear();
	}

	//trit_reference constructor
	TritSet::TritReference::TritReference(TritSet& trit_set, size_t position) : _trit_set(trit_set), _position(position) {}
	//----------------------------------------------------------------------------------------------------------------------------------------
	
	//overloading
	//----------------------------------------------------------------------------------------------------------------------------------------
	//overloading '[]' Lvalue
	TritSet::TritReference TritSet::operator [] (size_t index) {
		return TritReference(*this, index);
	}
	//overloading '[]' Rvalue
	Trit TritSet::operator [] (size_t position) const {
		return _get_trit(position);
	}
	//overloading TritSet = trit
	TritSet& TritSet::TritReference::operator = (Trit trit) {
		_trit_set._set_trit(trit, _position);
		return _trit_set;
	}
	//overloading setA = setB;
	TritSet& TritSet::operator = (const TritSet& trit_set) {
		_container.clear();

		_vector_size = trit_set._vector_size;
		_container_size = trit_set._container_size;
		_last_significant_trit = trit_set._last_significant_trit;

		_true_counter = trit_set._true_counter;
		_false_counter = trit_set._false_counter;
		_unknown_counter = trit_set._unknown_counter;

		for (size_t i = 0; i < _vector_size; i++)
		{
			_container.push_back(0);
		}
		for (size_t i = 0; i < _last_significant_trit; i++)
		{
			_set_trit(trit_set._get_trit(i), i);
		}

		return *this;
	}

	//overloading setA = setB & setC (example)
	TritSet& TritSet::operator = (TritSet&& trit_set){
		_container.clear();
		_vector_size = trit_set._vector_size;
		_container_size = trit_set._container_size;
		_last_significant_trit = trit_set._last_significant_trit;

		_true_counter = trit_set._true_counter;
		_false_counter = trit_set._false_counter;
		_unknown_counter = trit_set._unknown_counter;

		_container = trit_set._container;

		trit_set._vector_size = 0;
		trit_set._container_size = 0;
		trit_set._last_significant_trit = 0;

		trit_set._true_counter = 0;
		trit_set._false_counter = 0;
		trit_set._unknown_counter = 0;

		trit_set._container.clear();

		return *this;
	}

	bool operator == (const TritSet& trit_set_1, const TritSet& trit_set_2) {
		if (trit_set_1.size() != trit_set_2.size()) {
			return false;
		}
		for (size_t i = 0; i < trit_set_1.size(); i++) {
			if (trit_set_1[i] != trit_set_2[i]) {
				return false;
			}
		}
		return true;
	}

	bool operator != (const TritSet& trit_set_1, const TritSet& trit_set_2) {
		return !(trit_set_1 == trit_set_2);
	}

	TritSet operator ~ (const TritSet& trit_set) {
		TritSet new_trit_set(trit_set.size());

		for (size_t i = 0; i < trit_set.size(); i++) {
			new_trit_set[i] = ~trit_set[i];
		}
		
		return new_trit_set;
	}

	TritSet operator | (const TritSet& trit_set_1, const TritSet& trit_set_2) {
		size_t new_container_size = trit_set_1.size() > trit_set_2.size() ? trit_set_1.size() : trit_set_2.size();

		TritSet new_trit_set(new_container_size);

		for (size_t i = 0; i < new_container_size; i++) {
			new_trit_set[i] = trit_set_1[i] | trit_set_2[i];
		}

		return new_trit_set;
	}

	TritSet operator & (const TritSet& trit_set_1, const TritSet& trit_set_2) {
		size_t new_container_size = trit_set_1.size() > trit_set_2.size() ? trit_set_1.size() : trit_set_2.size();
		TritSet new_trit_set(new_container_size);

		for (size_t i = 0; i < new_container_size; i++) {
			new_trit_set[i] = trit_set_1[i] & trit_set_2[i];
		}

		return new_trit_set;
	}
	//----------------------------------------------------------------------------------------------------------------------------------------
	
	//some additional methods
	//----------------------------------------------------------------------------------------------------------------------------------------
	size_t TritSet::cardinality(Trit trit) const {
		if (trit == TRUE) { 
			return _true_counter; 
		}
		else if (trit == FALSE) {
			return  _false_counter; 
		}
		else {
			return _unknown_counter;
		}
	}

	std::unordered_map<Trit, size_t, std::hash<size_t>> TritSet::cardinality() const {
		std::unordered_map<Trit, size_t, std::hash<size_t>> unordered_map;
		unordered_map[TRUE] = _true_counter;
		unordered_map[FALSE] = _false_counter;
		unordered_map[UNKNOWN] = _unknown_counter;
		return unordered_map;
	}

	void TritSet::shrink() {
		if (!_vector_size) {
			return;
		}
		long long last_significant_index = _search_last_index();

		_reallocate_trit_set(last_significant_index);
		_container_size = (last_significant_index + Consts::LEVELER);
		_last_significant_trit = last_significant_index + Consts::LEVELER;
	}
	
	void TritSet::trim(size_t last_significant_index) {

		_reallocate_trit_set(last_significant_index);

		size_t container_position = last_significant_index / (Consts::UINT_SIZE * Consts::BYTE_SIZE / Consts::TRIT_SIZE);

		uint shift = Consts::UINT_SIZE * Consts::BYTE_SIZE / - (last_significant_index + Consts::LEVELER) * Consts::TRIT_SIZE;

		_container[container_position] = (_container[container_position] >> shift) << shift;
		_last_significant_trit = _search_last_index() + Consts::LEVELER;
	}
	//----------------------------------------------------------------------------------------------------------------------------------------


};


