#pragma once
#include <string>

namespace Consts {
	static const int TRIT_SIZE = 2;
	static const int BYTE_SIZE = 8;
	static const int LEVELER = 1;
	static const int MASK = 0x3;
	static const int UINT_SIZE = sizeof(unsigned int);
	static const char TRUE = 't';
	static const char FALSE = 'f';
	static const char UNKNOWN = 'u';

}