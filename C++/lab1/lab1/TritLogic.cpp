#include "TritLogic.h"

namespace TritLogic {

	Trit operator ~ (Trit trit) {
		if (trit == FALSE) {
			return TRUE;
		}
		else if (trit == TRUE) {
			return FALSE;
		}
		else {
			return UNKNOWN;
		}
	}

	Trit operator & (Trit trit_1, Trit trit_2) {
		if (trit_1 == FALSE || trit_2 == FALSE) {
			return FALSE;
		}
		else if (trit_1 == UNKNOWN || trit_2 == UNKNOWN) {
			return UNKNOWN;
		}
		else {
			return TRUE;
		}
	}

	Trit operator | (Trit trit_1, Trit trit_2) {
		if (trit_1 == TRUE || trit_2 == TRUE) {
			return TRUE;
		}
		else if (trit_1 == UNKNOWN || trit_2 == UNKNOWN) {
			return UNKNOWN;
		}
		else {
			return FALSE;
		}
	}
};