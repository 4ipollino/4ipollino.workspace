#pragma once
#include "Context.h"

namespace Workers {

	class SortWorker : public Interfaces::Worker {
	private:
		//worker's discription
		Context::Context& _context;
		//worker's types
		WorkerTypes::Type _in_type = WorkerTypes::TEXT;
		WorkerTypes::Type _out_type = WorkerTypes::TEXT;
	public:
		//constructor
		SortWorker(std::vector<std::wstring>& worker_discription, Context::Context& context);
		//destructor
		//~SortWorker();
		//main methods
		void work() override;
		//getter
		WorkerTypes::Type get_in_type() const override;
		WorkerTypes::Type get_out_type() const override;
	};
}
