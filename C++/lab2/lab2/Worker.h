#pragma once

#include <fstream>
#include <vector>
#include <map>
#include <string>

#include "Utils.h"
#include "Context.h"
#include "Consts.h"

#include "ArgumentExeption.h"
#include "BadInputFileExeption.h"
#include "IncompatibleWorkersTypes.h"
#include "BadSyntaxExeption.h"
#include "MemoryLeakExeption.h"
#include "BadOutputFileExeption.h"

namespace Interfaces {
	
	class Worker {
	public:
		virtual void work() = 0;
		
		virtual WorkerTypes::Type get_in_type() const = 0;
		virtual WorkerTypes::Type get_out_type() const = 0;
	};
}

