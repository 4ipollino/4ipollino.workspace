#pragma once
#include <stdexcept>

namespace Errors {

	class ArgumentExeption : public std::runtime_error {
	public:
		explicit ArgumentExeption(char const* const message) noexcept : runtime_error(message) {}
	};
}