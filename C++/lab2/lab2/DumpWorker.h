#pragma once
#include <fstream>
#include "Context.h"

namespace Workers {

	class DumpWorker : public Interfaces::Worker {
	private:
		//worker's discription
		std::wstring _file_name;
		std::wofstream _out_file;
		Context::Context& _context;
		//worker's types
		WorkerTypes::Type _in_type = WorkerTypes::TEXT;
		WorkerTypes::Type _out_type = WorkerTypes::TEXT;
	public:
		//constructor
		DumpWorker(std::vector<std::wstring>& worker_discription, Context::Context& context);
		//destructor
		~DumpWorker();
		//main methods
		void work() override;
		//getters
		WorkerTypes::Type get_in_type() const override;
		WorkerTypes::Type get_out_type() const override;
	};
}
