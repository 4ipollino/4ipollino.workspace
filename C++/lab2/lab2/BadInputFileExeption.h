#pragma once
#include <stdexcept>

namespace Errors {

	class BadInputFileExeption : public std::runtime_error {
	public:
		explicit BadInputFileExeption(char const* const message) noexcept : runtime_error(message) {}
	};
}