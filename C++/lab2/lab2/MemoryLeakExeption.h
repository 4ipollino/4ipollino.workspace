#pragma once
#include <stdexcept>

namespace Errors {

	class MemoryLeakExeption : public std::runtime_error {
	public:
		explicit MemoryLeakExeption(char const* const message) noexcept : runtime_error(message) {}
	};
}