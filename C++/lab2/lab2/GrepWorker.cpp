#include "GrepWorker.h"

namespace Workers {
	
	//getters
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	WorkerTypes::Type GrepWorker::get_in_type() const {
		return _in_type;
	}
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	WorkerTypes::Type GrepWorker::get_out_type() const {
		return _out_type;
	}
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

	//constructor
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	GrepWorker::GrepWorker(std::vector<std::wstring>& worker_discription, Context::Context& context) : _context(context) {
		if (worker_discription.size() != Consts::AMOUNT_GREP_WORKER_ARGS) {
			std::string error_text = Consts::BAD_AMOUNT_OF_WORKER_ARGS_ERROR;
			throw Errors::ArgumentExeption(error_text.c_str());
		}
		_word = worker_discription[Consts::GREP_WORD];
	}
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

	//main methods
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	void GrepWorker::work() {
		std::wstring grepped_text = Consts::WEMPTY_STR;
		std::wstring line = Consts::WEMPTY_STR;
		std::wstring text = _context.get_text();
		
		for (size_t i = 0; i < text.size(); ++i) {
			if (text[i] != Consts::WENDL) {
				line += text[i];
			}
			else {
				if (line.find(_word) == std::string::npos) {
					line = Consts::WEMPTY_STR;
					continue;
				}
				grepped_text += line + Consts::WENDL;
				line = Consts::WEMPTY_STR;
			}
		}
		_context.set_text(grepped_text);
	}
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
}