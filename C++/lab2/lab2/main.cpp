#include "Context.h"
#include "Factory.h"
#include "WorkFlowParser.h"
#include "WorkFlowExecutor.h"
#include "Consts.h"
#include "ConcreteFactory.h"

#include "ReadFileCreator.h"
#include "WriteFileCreator.h"
#include "DumpCreator.h"
#include "GrepCreator.h"
#include "SortCreator.h"
#include "ReplaceCreator.h"
#include <iostream>

int main(int argc, char** argv) {

	std::map<std::wstring, Interfaces::Creator*> creators;
	creators[Consts::READ] = new Creators::ReadFileCreator;
	creators[Consts::DUMP] = new Creators::DumpCreator;
	creators[Consts::REPLACE] = new Creators::ReplaceCreator;
	creators[Consts::WRITE] = new Creators::WriteFileCreator;
	creators[Consts::GREP] = new Creators::GrepCreator;
	creators[Consts::SORT] = new Creators::SortCreator;

	Context::Context context;

	Factorys::ConcreteFactory factory(creators, context);

	WorkFlow::WorkFlowParser parser(argv[Consts::IN_FILE_ARG], factory);

	WorkFlow::WorkFlowExecutor executor(parser);
	
	try {
		executor.run_workflow();
	}
	catch (const std::exception& ex) {
		std::cout << ex.what() << std::endl;
		for (auto x : creators) {
			delete x.second;
		}
		return EXIT_FAILURE;
	}


	for (auto x : creators) {
		delete x.second;
	}

	return EXIT_SUCCESS;
}