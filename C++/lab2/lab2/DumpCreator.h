#pragma once

#include "Creator.h"
#include "DumpWorker.h"

namespace Creators {

	class DumpCreator : public Interfaces::Creator {
	public:
		//main methods
		Interfaces::Worker* create_worker(std::vector<std::wstring>& worker_discription, Context::Context& context) const override;
	};
}
 