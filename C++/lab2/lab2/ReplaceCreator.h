#pragma once

#include "Creator.h"
#include "ReplaceWorker.h"

namespace Creators {

	class ReplaceCreator : public Interfaces::Creator {
	public:
		//main methods
		Interfaces::Worker* create_worker(std::vector<std::wstring>& worker_discription, Context::Context& context) const override;
	};
}
