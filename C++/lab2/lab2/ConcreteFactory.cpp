#include "ConcreteFactory.h"
#include <iostream>
namespace Factorys {
	
	//additional methods
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	void ConcreteFactory::_clear_map(std::map<int, Interfaces::Worker*>& workers) {
		if (workers.size()) {
			for (auto it : workers) {
				delete it.second;
			}
		}
	}
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	//constructors
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	ConcreteFactory::ConcreteFactory(std::map<std::wstring, Interfaces::Creator*>& creators, Context::Context& context) :
		_creators(creators), 
		_context(context) 
	{}
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	//main factory method
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	std::map<int, Interfaces::Worker*> ConcreteFactory::create_workers(std::vector<std::vector<std::wstring>>& workers_discription) {
		std::map<int, Interfaces::Worker*> workers;

		for (size_t i = 0; i < workers_discription.size(); ++i) {
			if (_creators.find(workers_discription[i][Consts::COMMAND_NAME]) == _creators.end()) {
				std::string error_text = Consts::BAD_COMMAND_NAME_ERROR;
				_clear_map(workers);
				throw Errors::BadSyntaxExeption(error_text.c_str());
			}
			//std::cout << Utils::wstring_to_string(workers_discription[i][Consts::COMMAND_NAME]);
			int index = std::stoi(Utils::wstring_to_string(workers_discription[i][Consts::WORKER_ID]));
			if (workers.find(index) != workers.end()) {
				_clear_map(workers);
				std::string error_text = Consts::BAD_ID_WORKFLOW_ERROR + std::to_string(index);
				throw Errors::BadSyntaxExeption(error_text.c_str());
			}
			
			workers[index] = _creators[workers_discription[i][Consts::COMMAND_NAME]]->create_worker(workers_discription[i], _context);
		}
		return workers;
	}
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
}