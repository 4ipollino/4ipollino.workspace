#pragma once
#include <string>

namespace Consts {

	static const int AMOUNT_READ_FILE_WORKER_ARGS = 4;
	static const int AMOUNT_WRITE_FILE_WORKER_ARGS = 4;
	static const int AMOUNT_SORT_WORKER_ARGS = 3;
	static const int AMOUNT_GREP_WORKER_ARGS = 4;
	static const int AMOUNT_DUMP_WORKER_ARGS = 4;
	static const int AMOUNT_REPLACE_WORKER_ARGS = 5;

	static const int FILE_NAME = 3;
	static const int COMMAND_NAME = 2;
	static const int WORKER_ID = 0;
	static const int IN_FILE_ARG = 1;
	static const int GREP_WORD = 3;
	static const int REPLACE_WORD1 = 3;
	static const int REPLACE_WORD2 = 4;

	static const int EQ = 1;
	static const int LOWER_BOUND_FOR_INT = 0;

	static const wchar_t WENDL = '\n';
	static const std::wstring WEQ = L"=";
	static const std::wstring START_WORKFLOW_CODE = L"desc";
	static const std::wstring END_WORKFLOW_CODE = L"csed";
	static const std::string EXPECTED1 = " -> expected: 'desc'";
	static const std::string EXPECTED2 = " -> expected: 'csed'";
	static const std::string SEPARATOR = " ";
	static const wchar_t WSEPARATOR = L' ';
	static const std::wstring WEMPTY_STR = L"";
	static const std::wstring WARROW = L"->";
	static const std::vector<std::wstring> WEMPTY_VECT = {};
	
	static const std::string BAD_OUTPUT_FILE_DUMP_ERROR = "bad output file in 'sort', fix -> file: ";
	static const std::string BAD_OUTPUT_FILE_WRITE_WORKER_ERROR = "Bad output file in 'writefile', fix -> file: ";
	static const std::string MEMORY_LEAK_ERROR = "You are trying to loose data, first make 'writefile' -> 'readfile', or fix your workflow order -> try again";
	static const std::string BAD_COMMAND_NAME_ERROR = "There is no such command name -> fix -> try again.";
	static const std::string BAD_AMOUNT_OF_WORKER_ARGS_ERROR = "Bad amount of worker's args -> fix -> try again.";
	static const std::string BAD_INPUT_FILE_READ_WORKER_ERROR = "Bad input file in 'readfile', fix -> file: ";
	static const std::string BAD_INPUT_FILE_WORKFLOW_ERROR = "Bad input workflow file, fix -> file: ";
	static const std::string BAD_SYNTAX_WORKFLOW_ERROR = "There are some probles with syntax - > fix -> try again ";
	static const std::string INCOMPATIBLE_WORKERS_TYPES_ERROR = "There are some problems with workers types in your workflow order -> fix -> try again";
	static const std::string BAD_ID_WORKFLOW_ERROR = "There are some problems with existing such id or there are already exists such id, check your workflow -> fix - > try again ";
	
	
	static const std::wstring READ = L"readfile";
	static const std::wstring WRITE = L"writefile";
	static const std::wstring GREP = L"grep";
	static const std::wstring SORT = L"sort";
	static const std::wstring REPLACE = L"replace";
	static const std::wstring DUMP = L"dump";
}