#pragma once
#include <stdexcept>

namespace Errors {

	class BadSyntaxExeption : public std::runtime_error {
	public:
		explicit BadSyntaxExeption(char const* const message) noexcept : runtime_error(message) {}
	};
}