#pragma once
#include "Factory.h"
#include "Creator.h" 
		
namespace Factorys {
	
	class ConcreteFactory : public Interfaces::Factory {
	private:
		//main fields
		std::map<std::wstring, Interfaces::Creator*>& _creators;
		Context::Context& _context;
		
		//setter
		void _clear_map(std::map<int, Interfaces::Worker*>& workers);
	public:
		//constructor
		ConcreteFactory(std::map<std::wstring, Interfaces::Creator*>& creators, Context::Context& context);
		//main factory method
		std::map<int, Interfaces::Worker*> create_workers(std::vector<std::vector<std::wstring>>& workers_discription)override;
	};
}
