#pragma once
#include "Worker.h"


namespace Workers {

	class ReadFileWorker : public Interfaces::Worker {
	private:
		//workers's discription
		std::wstring _file_name;
		std::wifstream _in_file;
		Context::Context& _context;
		//worker's types
		WorkerTypes::Type _in_type = WorkerTypes::EMPTY;
		WorkerTypes::Type _out_type = WorkerTypes::TEXT;
	public:
		//constructor
		ReadFileWorker(std::vector<std::wstring>& worker_discription, Context::Context& context);
		//destructor
		~ReadFileWorker();
		//main methods
		void work() override;
		//getters
		WorkerTypes::Type get_in_type() const override;
		WorkerTypes::Type get_out_type() const override;
	};
}
