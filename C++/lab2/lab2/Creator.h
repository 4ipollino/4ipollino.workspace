#pragma once
#include "Context.h"

namespace Interfaces {

	class Creator {
	public:
		virtual Worker* create_worker(std::vector<std::wstring>& worker_discriprion, Context::Context& context) const = 0;
	};
}
