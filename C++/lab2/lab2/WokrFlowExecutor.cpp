#include "WorkFlowExecutor.h"
#include <iostream>

namespace WorkFlow {

	//constructor
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	WorkFlowExecutor::WorkFlowExecutor(WorkFlowParser& parser) : _parser(parser) {}
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	
	//additional methods
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	void WorkFlowExecutor::_clear_map(std::map<int, Interfaces::Worker*>& m) {
		for (auto x : m) {
			delete x.second;
		}
	}
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	
	//workflow runner
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	void WorkFlowExecutor::run_workflow() {

		std::map<int, Interfaces::Worker*> workers;
		std::vector<int> workers_queue;
		
		try {
			workers = _parser.parse_file();
			workers_queue = _parser.get_workers_queue();
		}
		catch (std::exception& ex) {
			throw ex;
		}

		try {
			for (size_t i = 0; i < workers_queue.size(); ++i) {
				workers[workers_queue[i]]->work();
			}
		}
		catch (std::exception& ex) {
			std::cout << ex.what() << std::endl;
			_clear_map(workers);
			throw ex;
		}
		_clear_map(workers);
	}
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
}