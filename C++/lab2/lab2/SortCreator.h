#pragma once

#include "Creator.h"
#include "SortWorker.h"

namespace Creators {
	
	class SortCreator : public Interfaces::Creator {
	public:
		//main methods
		Interfaces::Worker* create_worker(std::vector<std::wstring>& worker_discription, Context::Context& context) const override;
	};
}
