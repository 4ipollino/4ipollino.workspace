#pragma once

#include <string>
#include <codecvt>

namespace Utils {
	std::string wstring_to_string(std::wstring str);
}

namespace WorkerTypes {
	
	enum Type {
		EMPTY = 0,
		TEXT = 1
	};
}