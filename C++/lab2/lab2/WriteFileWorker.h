#pragma once
#include "Context.h"

namespace Workers {

	class WriteFileWorker : public Interfaces::Worker {
	private:
		//worker's discription
		Context::Context& _context;
		std::wstring _file_name;
		std::wofstream _out_file;
		//worker's types
		WorkerTypes::Type _in_type = WorkerTypes::TEXT;
		WorkerTypes::Type _out_type = WorkerTypes::EMPTY;
	public:
		//constructor
		WriteFileWorker(std::vector<std::wstring>& worker_discription, Context::Context& context);
		//destructor
		~WriteFileWorker();
		//main methods
		void work() override;
		//getters
		WorkerTypes::Type get_in_type() const override;
		WorkerTypes::Type get_out_type() const override;
	};
}
