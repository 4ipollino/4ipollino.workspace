#pragma once

#include "Worker.h"

namespace Interfaces {

	class Factory {
	public:
		virtual std::map<int, Interfaces::Worker*> create_workers(std::vector<std::vector<std::wstring>>& worker_discription) = 0;
	};
}

