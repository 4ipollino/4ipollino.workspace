#pragma once
#include "Context.h"

namespace Workers {

	class ReplaceWorker : public Interfaces::Worker {
	private:
		//worker's discription
		std::wstring _word1;
		std::wstring _word2;
		Context::Context& _context;
		//worker's types
		WorkerTypes::Type _in_type = WorkerTypes::TEXT;
		WorkerTypes::Type _out_type = WorkerTypes::TEXT;
	public:
		//constructor
		ReplaceWorker(std::vector<std::wstring>& worker_discription, Context::Context& context);
		//destructor
		//~ReplaceWorker();
		//main methods
		void work() override;
		//getters
		WorkerTypes::Type get_in_type() const override;
		WorkerTypes::Type get_out_type() const override;
	};
}
