#pragma once

#include "WorkFlowParser.h"

namespace WorkFlow {
	
	class WorkFlowExecutor{
	private:
		WorkFlowParser& _parser;
		
		//additional methods
		void _clear_map(std::map<int, Interfaces::Worker*>&);
	public:
		WorkFlowExecutor(WorkFlowParser& parser);
 
		void run_workflow();
	};
}