#include "WorkFlowParser.h"
#include <iostream>
namespace WorkFlow {
	
	//additional methods
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	void WorkFlowParser::_clear_map(std::map<int, Interfaces::Worker*>& workers) {
		if (workers.size()) {
			for (auto it : workers) {
				delete it.second;
			}
		}
	}
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	
	//getter
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	std::vector<int> WorkFlowParser::get_workers_queue() const {
		return _workers_queue;
	}
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	
	//cunstructor
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	WorkFlowParser::WorkFlowParser(const std::string file_name, Interfaces::Factory& factory) : _factory(factory) {
		_file_name = file_name;
		_workers_queue = {};
	}
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	
	//destructor
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	WorkFlowParser::~WorkFlowParser() {
		_in_file.close();
	}
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	
	//parsing
	//lil line parser
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	std::vector<std::wstring> WorkFlowParser::_parse_line(std::wstring line) {
		std::vector<std::wstring> res;
		std::wstring discription = Consts::WEMPTY_STR;
		for (size_t i = 0; i < line.size(); ++i) {
			if (line[i] != Consts::WSEPARATOR) {
				discription += line[i];
			}
			else {
				res.push_back(discription);
				discription = Consts::WEMPTY_STR;
			}
		}
		res.push_back(discription);
		return res;
	}
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	
	//lil workflow parser
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	void WorkFlowParser::_parse_workflow(std::map<int, Interfaces::Worker*>& workers) {

		std::wstring workflow_line = Consts::WEMPTY_STR;
		size_t id;

		getline(_in_file, workflow_line);
		std::vector<std::wstring> workflow = _parse_line(workflow_line);
		
		WorkerTypes::Type prev_worker_out_type = WorkerTypes::Type::EMPTY;
		for (size_t i = 0; i < workflow.size(); i++) {
			if (i % 2 == 0) {
				try {
					id = stoi(workflow[i]);
				}
				catch (...) {
					_clear_map(workers);
					std::string error_text = Consts::BAD_SYNTAX_WORKFLOW_ERROR + Utils::wstring_to_string(workflow_line);
					throw Errors::BadSyntaxExeption(error_text.c_str());
				}
				
				if (workers.find(id) == workers.end()) {
					_clear_map(workers);
					std::string error_text = Consts::BAD_ID_WORKFLOW_ERROR;
					throw Errors::BadSyntaxExeption(error_text.c_str());
				}
				
				if (workers[id]->get_in_type() != prev_worker_out_type) {
					_clear_map(workers);
					std::string error_text = Consts::INCOMPATIBLE_WORKERS_TYPES_ERROR;
					throw Errors::BadSyntaxExeption(error_text.c_str());
				}
				prev_worker_out_type = workers[id]->get_out_type();
				_workers_queue.push_back(id);
			}
			else {
				if (workflow[i] != Consts::WARROW) {
					_clear_map(workers);
					std::string error_text = Consts::BAD_SYNTAX_WORKFLOW_ERROR;
					throw Errors::BadSyntaxExeption(error_text.c_str());
				}
			}
		}
	}
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ 
	
	//main parser
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	std::map<int, Interfaces::Worker*> WorkFlowParser::parse_file() {
		const std::locale utf8_locale = std::locale(std::locale(), new std::codecvt_utf8<wchar_t>());
		_in_file.open(_file_name);
		_in_file.imbue(utf8_locale);

		if (!_in_file.is_open()) {
			std::string error_text = Consts::BAD_INPUT_FILE_WORKFLOW_ERROR + _file_name;
			throw Errors::BadInputFileExeption(error_text.c_str());
		}

		std::wstring line = Consts::WEMPTY_STR;
		size_t lines_counter = 0;

		getline(_in_file, line);
		++lines_counter;
		//checing "desc" code word
		if (line != Consts::START_WORKFLOW_CODE) {
			std::string error_text = Consts::BAD_SYNTAX_WORKFLOW_ERROR + std::to_string(lines_counter) + Consts::SEPARATOR + Utils::wstring_to_string(line) + Consts::EXPECTED1;
			throw Errors::BadSyntaxExeption(error_text.c_str());
		}
		size_t id = 0;
		getline(_in_file, line);
		++lines_counter;

		std::vector<std::vector<std::wstring>> parsed_discription;

		while (line != Consts::END_WORKFLOW_CODE || _in_file.eof()) {
			//checking "csed" code word
			if (_in_file.eof()) {
				std::string error_text = Consts::BAD_SYNTAX_WORKFLOW_ERROR + std::to_string(lines_counter) + Consts::SEPARATOR + Utils::wstring_to_string(line) + Consts::EXPECTED2;
				throw Errors::BadSyntaxExeption(error_text.c_str());
			}

			std::vector<std::wstring> discription = _parse_line(line);
			try {
				id = stoi(discription[Consts::WORKER_ID]);
			}
			catch (...) {
				std::string error_text = Consts::BAD_SYNTAX_WORKFLOW_ERROR + std::to_string(lines_counter) + Consts::SEPARATOR + Utils::wstring_to_string(line);
				throw Errors::BadSyntaxExeption(error_text.c_str());
			}
	
			if (id < Consts::LOWER_BOUND_FOR_INT) {
				std::string error_text = Consts::BAD_SYNTAX_WORKFLOW_ERROR + std::to_string(lines_counter) + Consts::SEPARATOR + Utils::wstring_to_string(line);
				throw Errors::BadSyntaxExeption(error_text.c_str());
			}

			if (discription[Consts::EQ] != Consts::WEQ) {
				std::string error_text = Consts::BAD_SYNTAX_WORKFLOW_ERROR + std::to_string(lines_counter) + Consts::SEPARATOR + Utils::wstring_to_string(line);
				throw Errors::BadSyntaxExeption(error_text.c_str());
			}

			parsed_discription.push_back(discription);
			getline(_in_file, line);
			++lines_counter;
		}

		std::map<int, Interfaces::Worker*> workers;
		
		try {
			workers = _factory.create_workers(parsed_discription);
		}
		catch (std::exception& ex) {
			throw Errors::BadSyntaxExeption(ex.what());
		}

		// Read last string anp parse it. Create ids list
		try {
			_parse_workflow(workers);
		}
		catch (const std::exception& ex) {
			throw ex;
		}

		return workers;

	}
	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
}