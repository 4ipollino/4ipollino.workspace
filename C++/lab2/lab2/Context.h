#pragma once

#include "Worker.h"

namespace Context {

	class Context {
	private:
		//context discription
		std::wstring _text;

	public:
		//getter
		std::wstring get_text() const;
		//setter
		void set_text(std::wstring text);
	};
}

