#pragma once
#include "Worker.h"
#include "Factory.h"
namespace WorkFlow {

	class WorkFlowParser {
	private:
		std::string _file_name;
		std::wifstream _in_file;

		std::vector<int> _workers_queue;
		Interfaces::Factory& _factory;

		//utils, Vasya said that you said that making very big file parser it's not good, so that's a lil methods of parsing line and workflow
		std::vector<std::wstring> _parse_line(std::wstring line);
		void _parse_workflow(std::map<int, Interfaces::Worker*>& workers);
		void _clear_map(std::map<int, Interfaces::Worker*>& workers);
	public:
		//getter
		std::vector<int> get_workers_queue() const;
		
		//constructor
		WorkFlowParser(const std::string file_name, Interfaces::Factory& factory);
		
		//destructor
		~WorkFlowParser();
		//parse
		std::map<int, Interfaces::Worker*> parse_file();
	};
}