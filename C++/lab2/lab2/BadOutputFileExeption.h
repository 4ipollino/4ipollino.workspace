#pragma once
#include <stdexcept>

namespace Errors {

	class BadOutputFileExeption : public std::runtime_error {
	public:
		explicit BadOutputFileExeption(char const* const message) noexcept : runtime_error(message) {}
	};
}